#include <string.h>
#include <sstream>
#include <errno.h>
#include <string.h>

#include <unistd.h>
#include <poll.h>
#include <sys/socket.h>
#include <arpa/inet.h>

#include <chrono>
#include <thread>

#include "simple_fwd.h"
#include "ppapi/cpp/host_resolver.h"
#include "ppapi/utility/completion_callback_factory.h"

extern int set_non_blocking(int sock, int onoff);

extern int empty_cb (proxygen::http_parser *p);
extern int empty_data_cb (proxygen::http_parser *p, const char *buf, size_t len);

static proxygen::http_parser_settings body_complete_settings =
  {.on_message_begin = empty_cb
  ,.on_header_field = empty_data_cb
  ,.on_header_value = empty_data_cb
  ,.on_url = empty_data_cb
  ,.on_body = empty_data_cb
  ,.on_headers_complete = empty_data_cb
  ,.on_message_complete = simple_forwarder_t::on_message_complete_cb
  ,.on_reason = empty_data_cb
  ,.on_chunk_header = empty_cb
  ,.on_chunk_complete = empty_cb
};


simple_forwarder_t::simple_forwarder_t(pp::Instance *instance, std::string host, uint16_t port, int cli_sock)
:instance_(instance)
,connected_(false)
,msg_complete_(false)
,flow_(CLI2HOST)
,cli_sock_(cli_sock)
,host_sock_(0)
{
    std::string str_addr = "unknown";
    std::ostringstream s;
    struct sockaddr_in addr = {0};

    proxygen::http_parser::data = this;
    proxygen::http_parser_init(this, proxygen::HTTP_RESPONSE);
    
    //FIXME: replase this with POSIX gethostbyname-->
    pp::CompletionCallback cb;
    pp::HostResolver resolver(instance_);
    
    PP_HostResolver_Hint hint = { PP_NETADDRESS_FAMILY_UNSPECIFIED, 0 };
    resolver.Resolve(host.c_str(), port, hint, cb);
    
    pp::NetAddress net_addr = resolver.GetNetAddress(0);
    str_addr = net_addr.DescribeAsString(false).AsString();
    //<--

    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = inet_addr(str_addr.c_str());
    addr.sin_port        = htons(port);
    
    if(-1 == (host_sock_ = ::socket(AF_INET, SOCK_STREAM, 0))) {
        s << "failed to create client socket" << std::endl;
        s << strerror(errno) << std::endl;
        instance_->PostMessage(s.str());
        return;
    } else if(0 != ::connect(host_sock_, (struct sockaddr*)&addr, sizeof(addr))) {
        s << "posix sock - Failed to connect to " << str_addr << std::endl;
        s << "error: " << strerror(errno) << std::endl;
        instance_->PostMessage(s.str());
        return;      
    }
    connected_ = 1;
    set_non_blocking(cli_sock_, 1);
    set_non_blocking(host_sock_, 1);
    
    s << "Connected to " << host << "!" << std::endl;
    instance_->PostMessage(s.str());
}

simple_forwarder_t::~simple_forwarder_t()
{
    if(!host_sock_)
        return;

    ::shutdown(host_sock_, SHUT_RDWR);
    ::close(host_sock_);
    host_sock_ = 0;
}


int simple_forwarder_t::on_message_complete_cb(proxygen::http_parser *p)
{
    simple_forwarder_t *pthis = (simple_forwarder_t*)p;
    pthis->msg_complete_ = 1;
    return 0;
}

void simple_forwarder_t::operator()(const std::string &req)
{
    this->operator()(req.c_str(), req.length());
}

void simple_forwarder_t::operator()(const char *req, size_t len)
{
    std::ostringstream s;
    const char *name[2] = {"HOST", "CLI"};
    int sock[2] = {host_sock_, cli_sock_};
    char *buf[2] = {h2c, c2h};
    int bufsz[2] = {sizeof(h2c), sizeof(c2h)};
    int desired_flow[2] = {HOST2CLI, CLI2HOST};
    int force_flow[2] = {1, 0};

    if(!connected_) {
        s << "simple fwd: soket is not connected" << std::endl;
        instance_->PostMessage(s.str());
        return;
    }
    
    int32_t nwritten = ::write(host_sock_, req, len);

    if(nwritten != len) {
        s << "Failed to write " << len << " bytes to host, wrote "<< nwritten << std::endl;
        instance_->PostMessage(s.str());
        return;
    }
    s << "CLI-->[" << len << "]-->HOST" << std::endl;
    instance_->PostMessage(s.str());
    std::ostringstream().swap(s);

    
    struct pollfd fds[2];
    int timeout_ms = 1000;
    int done = 0;

    fds[0].fd = host_sock_;
    fds[0].events = POLLIN | POLLERR | POLLHUP | POLLNVAL;
    fds[1].fd = cli_sock_;
    fds[1].events = POLLIN | POLLERR | POLLHUP | POLLNVAL;
    
    while(!done) {
        
        auto r = ::poll(fds, 2, timeout_ms);

        if(0 == r) {
            //instance_->PostMessage("Poll timeout\n");
            continue;
        } else if(0 > r) {

            if(EAGAIN == errno || errno == EINTR)
                continue;

            s << "Request processor: error polling the socket: " << strerror(errno) << std::endl;
            instance_->PostMessage(s.str());
            std::ostringstream().swap(s);
            done = 1;
            continue;
        }

        if(fds[0].revents == POLLIN || POLLIN == fds[1].revents) {
            for(int i = 0; i < 2; ++i) {
                
                if(fds[i].revents != POLLIN) {
                    continue;
                }

                if(force_flow[i])
                    flow_ = (flow_t)desired_flow[i];

                if(desired_flow[i] != flow_) {
                    s << "Simple forwarder: can't read from " << name[i] << "data flow is now to opposite direction"<< std::endl;
                    instance_->PostMessage(s.str());
                    std::ostringstream().swap(s);
                    continue;
                }

                int nread = ::read(sock[i], buf[i], bufsz[i]);
                
                //s << "Read " << nread << " bytes from " << name[i] << std::endl;              


                set_non_blocking(sock[i^1], 0);
                int nwrote = ::write(sock[i^1], buf[i], nread);
                set_non_blocking(sock[i^1], 1);

                if(nread != nwrote) {
                    s << "Simple forwarder: can't write to " << name[i^1] << ", expected " << nread << ", wrote " << nwrote << std::endl;
                    s << "Error: " << strerror(errno) << std::endl;
                    instance_->PostMessage(s.str());
                    std::ostringstream().swap(s);
                    continue;
                }
                s << name[1] << (1 == i ? "-->" : "<--") << "[" << nwrote << "]" << (1 == i ? "-->" : "<--") << name[0] << std::endl; 
                instance_->PostMessage(s.str());
                std::ostringstream().swap(s);

                if(0 == i) { //host
                    int nparsed = proxygen::http_parser_execute(this, &body_complete_settings, buf[i], nread);
                    if(nparsed != nread) {
                        s << "Failed to parse req: read " << nread << ", parsed " << nparsed << std::endl;
                        instance_->PostMessage(s.str());
                        done = 1;
                        continue;

                    } else if(msg_complete_) {
                        s << "Simple forwarder: recvd complete message from the host!" << std::endl;
                        instance_->PostMessage(s.str());
                        done = 1;
                    }
                }
            }
       } else if(fds[0].revents || fds[1].revents) {
            //handle error/hangup on host sock
            for(int i = 0; i < 2; ++i) {
                s << "Simple forwarder: reading " << name[i] << "  socket returned ";
                if(fds[i].revents & POLLERR)
                    s << "POLLERR ";
                if(fds[i].revents & POLLHUP)
                    s << "POLLHUP ";
                if(fds[i].revents & POLLNVAL)
                    s << "POLLNVAL " << std::endl;
                instance_->PostMessage(s.str());
            }
            s << "errno: " << strerror(errno) << std::endl;
            done = 1;
        } 
    }

    instance_->PostMessage("simple fwd: EOF!\n");
    delete this;
}
