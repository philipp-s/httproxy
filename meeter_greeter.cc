// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.


#include <string.h>
#include <unistd.h>
#include <functional>
#include <sys/socket.h>
#include <netinet/in.h> 
#include <arpa/inet.h>

#include <sstream>

#include "meeter_greeter.h"
#include "request_processor.h"

#include "ppapi/c/pp_errors.h"
#include "ppapi/cpp/var.h"
#include "ppapi/utility/completion_callback_factory.h"

MeeterGreeter::MeeterGreeter(pp::Instance *instance, uint16_t port)
:instance_(instance)
,listening_socket_(0)
,port_(port)
,thread_pool_(THREAD_POOL_SIZE)
{
  std::function<void()> f = std::bind(&MeeterGreeter::run, this);
  thread_pool_.async(f);
}

MeeterGreeter::~MeeterGreeter()
{
    if(!listening_socket_)
        return;

    shutdown(listening_socket_, SHUT_RDWR);
    close(listening_socket_);
    listening_socket_ = 0;
}


void MeeterGreeter::run(void)
{
    std::ostringstream s;
    struct sockaddr_in addr = {0}, cli_addr = {0};

    if(0 >= (listening_socket_ = ::socket(AF_INET, SOCK_STREAM, 0))) {
        s << "MeeterGreeter: failed to create listening socket" << std::endl;
        instance_->PostMessage(s.str());
        return;
    }

    addr.sin_family = AF_INET; 
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    addr.sin_port = htons(port_);

    if(0 != ::bind(listening_socket_, (struct sockaddr*)&addr, sizeof(addr))) {
        s << "MeeterGreeter: failed to bind to port " << port_ << ", error " << strerror(errno) << std::endl;
        instance_->PostMessage(s.str());
        ::close(listening_socket_);
        listening_socket_ = 0;
        return;
    }

    if(0 != ::listen(listening_socket_, THREAD_POOL_SIZE)) {
        s << "MeeterGreeter: failed to listen to port " << port_ << std::endl;
        instance_->PostMessage(s.str());
        ::close(listening_socket_);
        listening_socket_ = 0;
        return;
    }

    instance_->PostMessage("Started listening...\n");

    while(1) {
        socklen_t len = sizeof(cli_addr);
        int cli_sock = ::accept(listening_socket_, (struct sockaddr*)&cli_addr, &len);

        if(-1 == cli_sock) {
            if(EINTR == errno){
                s << "MeeterGreeter: accept interrupted (EINTR), go on accepting..." << std::endl;
                instance_->PostMessage(s.str());
                continue;
            }
            s << "MeeterGreeter: failed accepting new connections!" << std::endl;
            instance_->PostMessage(s.str());
            return;
        }

        RequestProcessor *preq = new RequestProcessor(instance_, cli_sock);
        std::function<void()> f = std::bind(&RequestProcessor::run, preq);
        thread_pool_.async(f);
    }
}
#if 0
    :instance_(instance)
    ,callback_factory_(this)
    ,thread_pool_(THREAD_POOL_SIZE) {
    Start(port);
  }
#if 0
#ifdef WIN32
#undef PostMessage
#endif

// Number of connections to queue up on the listening
// socket before new ones get "Connection Refused"
static const int kBacklog = 10;

// Implement htons locally.  Even though this is provided by
// nacl_io we don't want to include nacl_io in this simple
// example.
static uint16_t Htons(uint16_t hostshort) {
  uint8_t result_bytes[2];
  result_bytes[0] = (uint8_t) ((hostshort >> 8) & 0xFF);
  result_bytes[1] = (uint8_t) (hostshort & 0xFF);

  uint16_t result;
  memcpy(&result, result_bytes, 2);
  return result;
}

void MeeterGreeter::Start(uint16_t port) {
  if (!pp::TCPSocket::IsAvailable()) {
    instance_->PostMessage("TCPSocket not available");
    return;
  }

  listening_socket_ = pp::TCPSocket(instance_);
  if (listening_socket_.is_null()) {
    instance_->PostMessage("Error creating TCPSocket.");
    return;
  }

  std::ostringstream status;
  status << "Starting server on port: " << port;
  instance_->PostMessage(status.str());

  // Attempt to listen on all interfaces (0.0.0.0)
  // on the given port number.
  PP_NetAddress_IPv4 ipv4_addr = { Htons(port), { 0 } };
  pp::NetAddress addr(instance_, ipv4_addr);
  pp::CompletionCallback callback =
      callback_factory_.NewCallback(&MeeterGreeter::OnBindCompletion);
  int32_t rtn = listening_socket_.Bind(addr, callback);
  if (rtn != PP_OK_COMPLETIONPENDING) {
    instance_->PostMessage("Error binding listening socket.");
    return;
  }
}

void MeeterGreeter::OnBindCompletion(int32_t result) {
  if (result != PP_OK) {
    std::ostringstream status;
    status << "meeter-greeter: Bind failed with: " << result << " ("<< strerror(-1*result) << ")";
    instance_->PostMessage(status.str());
    return;
  }

  pp::CompletionCallback callback =
      callback_factory_.NewCallback(&MeeterGreeter::OnListenCompletion);

  int32_t rtn = listening_socket_.Listen(kBacklog, callback);
  if (rtn != PP_OK_COMPLETIONPENDING) {
    instance_->PostMessage("meeter-greeter: Error listening on server socket.");
    return;
  }
}

void MeeterGreeter::OnListenCompletion(int32_t result) {
  std::ostringstream status;
  if (result != PP_OK) {
    status << "meeter-greeter: Listen failed with: " << result;
    instance_->PostMessage(status.str());
    return;
  }

  pp::NetAddress addr = listening_socket_.GetLocalAddress();
  status << "meeter-greeter: Listening on: " << addr.DescribeAsString(true).AsString();
  instance_->PostMessage(status.str());

  TryAccept();
}

void MeeterGreeter::OnAcceptCompletion(int32_t result, pp::TCPSocket socket) {
  std::ostringstream status;

  if (result != PP_OK) {
    status << "meeter-greeter: Accept failed: " << result;
    instance_->PostMessage(status.str());
    return;
  }

  pp::NetAddress addr = socket.GetLocalAddress();
  status << "meeter-greeter: New connection from: ";
  status << addr.DescribeAsString(true).AsString();
  instance_->PostMessage(status.str());
  
  RequestProcessor *preq = new RequestProcessor(instance_, socket);

  std::function<void()> f = std::bind(&RequestProcessor::run, preq);
  thread_pool_.async(f);

  TryAccept();
}

void MeeterGreeter::TryAccept() {
  pp::CompletionCallbackWithOutput<pp::TCPSocket> callback =
      callback_factory_.NewCallbackWithOutput(
          &MeeterGreeter::OnAcceptCompletion);
  listening_socket_.Accept(callback);
}
#endif
#endif
