#ifndef SIMPLE_FWD_H_
#define SIMPLE_FWD_H_

#include <atomic>
#include "ppapi/cpp/instance.h"
#include "http_parser/http_parser.h"

class simple_forwarder_t : public proxygen::http_parser
{
public:
    simple_forwarder_t(pp::Instance *instance, std::string host, uint16_t port, int cli_sock);
    virtual ~simple_forwarder_t();
    void operator()(const std::string &req);
    void operator()(const char *req, size_t len);
    static int on_message_complete_cb(proxygen::http_parser *p);
private:
    pp::Instance *instance_;
    bool connected_;
    bool msg_complete_;
    enum flow_t{
        CLI2HOST,
        HOST2CLI,
    } flow_;

private:
    int cli_sock_;
    int host_sock_;
    char c2h[4*1024];
    char h2c[4*1024];
};
#endif
