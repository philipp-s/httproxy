// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include "request_processor.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <errno.h>
#include <poll.h>
#include <fcntl.h>
#include <string.h>
#include <sstream>
#include <functional>

#include "ppapi/c/pp_errors.h"
#include "ppapi/cpp/var.h"
#include "ppapi/utility/completion_callback_factory.h"

int set_non_blocking(int sock, int onoff)
{
    int opts;

    if (0 > (opts = ::fcntl(sock, F_GETFL))) {
        return -1;
    }
    opts = onoff ? (opts | O_NONBLOCK) : (opts & ~O_NONBLOCK);
    return ::fcntl(sock, F_SETFL, opts);
}


RequestProcessor::RequestProcessor(pp::Instance *instance, int socket)
:instance_(instance)
,socket_(socket)
,host_("UNKNOWN")
,port_(80)
{
    set_non_blocking(socket_, 1);
    //socket_.SetOption(PP_TCPSOCKET_OPTION_NO_DELAY, flag, cb);
    proxygen::http_parser::data = this;
    proxygen::http_parser_init(this, proxygen::HTTP_REQUEST);
}

RequestProcessor::~RequestProcessor()
{
    ::shutdown(socket_, SHUT_RDWR);
    ::close(socket_);
}

int empty_cb (proxygen::http_parser *p) { return 0; }
int empty_data_cb (proxygen::http_parser *p, const char *buf, size_t len) { return 0; }

static proxygen::http_parser_settings settings_url =
  {.on_message_begin = empty_cb
  ,.on_header_field = empty_data_cb
  ,.on_header_value = empty_data_cb
  ,.on_url = RequestProcessor::url_cb
  ,.on_body = empty_data_cb
  ,.on_headers_complete = empty_data_cb
  ,.on_message_complete = empty_cb
  ,.on_reason = empty_data_cb
  ,.on_chunk_header = empty_cb
  ,.on_chunk_complete = empty_cb
};

void RequestProcessor::run(void)
{
    std::string req;
    std::ostringstream s;
    bool done = 0;
    char *begin = receive_buffer_, *end = receive_buffer_ + sizeof(receive_buffer_);
    struct pollfd fds[1];
    int timeout_ms = 1000;
    


    while(!done && begin < end) {
        int32_t r, nread, nparsed;

        fds[0].fd = socket_;
        fds[0].events = POLLIN | POLLERR | POLLHUP | POLLNVAL;
        
        r = ::poll(fds, 1, timeout_ms);

        if(0 == r) {
            //timeout
            continue;
        } else if(0 > r) {

            if(EAGAIN == errno || errno == EINTR)
                continue;

            s << "Request processor: error polling the socket: " << strerror(errno) << std::endl;
            instance_->PostMessage(s.str());
            return;
        }

        if(fds[0].revents != POLLIN) {
            
            s << "Request processor: polling the socket returned ";
            if(fds[0].revents & POLLERR)
                s << "POLLERR ";
            if(fds[0].revents & POLLHUP)
                s << "POLLHUP ";
            if(fds[0].revents & POLLNVAL)
                s << "POLLNVAL ";
            
            s << ", errno: " << strerror(errno) << std::endl;
            instance_->PostMessage(s.str());
            return;
        }

        nread = ::read(socket_, begin, end - begin);

        if(0 == nread){
            proxygen::http_parser_execute(this, &settings_url, begin, 0);//EOF
            instance_->PostMessage("EOF!\n");
            break;
        } else if(0 > nread) {
            s << "Request processor: read error - " << strerror(errno) << std::endl;
            instance_->PostMessage(s.str());
            return;
        }

        nparsed = proxygen::http_parser_execute(this, &settings_url, begin, nread);

        if(nparsed != nread) {
            s << "Failed to parse req: read " << nread << ", parsed " << nparsed << std::endl;
            instance_->PostMessage(s.str());
            break;
        }

        begin += nread;

        bool have_method = this->method != proxygen::HTTP_DELETE;

        if(have_method && have_url_){
#if 0
            switch(this->method) {
            case proxygen::HTTP_GET:
            {
                s << "Method: " << proxygen::http_method_str((enum proxygen::http_method)this->method) << std::endl;
                instance_->PostMessage(s.str());
                simple_forwarder_t fwd_to_host(instance_,host_, port_, socket_);
                fwd_to_host(receive_buffer_, nread);
            }
            break;
            default:
                s << "Method not supported: " << proxygen::http_method_str((enum proxygen::http_method)this->method) << std::endl;
                instance_->PostMessage(s.str());
            break;
            }
#else
            switch(this->method) {
            case proxygen::HTTP_CONNECT:
                s << "Method not supported: " << proxygen::http_method_str((enum proxygen::http_method)this->method) << std::endl;
                instance_->PostMessage(s.str());
                std::ostringstream().swap(s);
            break;
            default:
            {
                s << "Method: " << proxygen::http_method_str((enum proxygen::http_method)this->method) << std::endl;
                instance_->PostMessage(s.str());
                simple_forwarder_t fwd_to_host(instance_,host_, port_, socket_);
                fwd_to_host(receive_buffer_, nread);
            }
            break;
            }
#endif
            done = 1;
        }
    }
    
    if(!done) {
        s << "Request processor: read up to " << sizeof(receive_buffer_) << " bytes and got no HTTP..." << std::endl;
        instance_->PostMessage(s.str());
    }
    delete this;
}

int RequestProcessor::url_cb (proxygen::http_parser *parser, const char *at, size_t length)
{
    struct proxygen::http_parser_url url = {0};
    std::ostringstream os;
    std::string url_str(at, length);
    RequestProcessor *rp = (RequestProcessor*)parser;

    //os << "±±±±±±±±URL: " << url_str << std::endl;
    //rp->instance_->PostMessage(os.str());

    if(0 != proxygen::http_parser_parse_url(at, length, 0, &url)) {
        rp->instance_->PostMessage("failed to parse URL\n");
        return -1;
    }

    if(url.field_set & (1 << proxygen::UF_HOST)) {
        //os << "have host @ offset " << url.field_data[proxygen::UF_HOST].off << ", len " << url.field_data[proxygen::UF_HOST].len;
        //rp->instance_->PostMessage(os.str());
        rp->host_ = std::string(at + url.field_data[proxygen::UF_HOST].off, url.field_data[proxygen::UF_HOST].len);
    }

    if(url.field_set & (1 << proxygen::UF_PORT)) {
        //rp->instance_->PostMessage("have port\n");
        std::string port_str(at + url.field_data[proxygen::UF_PORT].off, url.field_data[proxygen::UF_PORT].len);
        rp->port_ = atoi(port_str.c_str());
    }

    os << "±±±±±±±±Host: " << rp->host_ << ":" << rp->port_ << std::endl;
    rp->instance_->PostMessage(os.str());

    rp->have_url_ = (url.field_set & (1 << proxygen::UF_HOST)) ? true : false;
    return rp->have_url_ ? 0 : -1;
}
