// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <stdio.h>
#include <string.h>
#include <sstream>

#include "meeter_greeter.h"

#include "ppapi/cpp/host_resolver.h"
#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/module.h"
#include "ppapi/cpp/tcp_socket.h"
//#include "ppapi/cpp/udp_socket.h"
#include "ppapi/cpp/var.h"
#include "ppapi/utility/completion_callback_factory.h"
#include "nacl_io/nacl_io.h"

class httproxy_instance_t : public pp::Instance {
 public:
  explicit httproxy_instance_t(PP_Instance instance)
    : pp::Instance(instance),
      callback_factory_(this),
      meeter_greeter_(NULL) {
   
        nacl_io_init_ppapi(instance, pp::Module::Get()->get_browser_interface());
    }


  virtual ~httproxy_instance_t() {
    if(meeter_greeter_) {
        delete meeter_greeter_;
        meeter_greeter_ = NULL;
    }
  }

  virtual void HandleMessage(const pp::Var& var_message);

 private:
  bool IsConnected();
  bool IsUDP();

  void Close();

  pp::CompletionCallbackFactory<httproxy_instance_t> callback_factory_;
  pp::HostResolver resolver_;
  pp::NetAddress remote_host_;

  MeeterGreeter  *meeter_greeter_;
};

#define MSG_CREATE_TCP 't'
//#define MSG_CREATE_UDP 'u'
//#define MSG_SEND 's'
#define MSG_CLOSE 'c'
#define MSG_LISTEN 'l'

void httproxy_instance_t::HandleMessage(const pp::Var& var_message) {
  if (!var_message.is_string())
    return;
  std::string message = var_message.AsString();
  // This message must contain a command character followed by ';' and
  // arguments like "X;arguments".
  if (message.length() < 2 || message[1] != ';')
    return;
  switch (message[0]) {
   case MSG_CLOSE:
        if(meeter_greeter_) {
            delete meeter_greeter_;
            meeter_greeter_ = NULL;
        }
      break;
    case MSG_LISTEN:
        if(meeter_greeter_) {
            PostMessage("Already listening!");
        } else {
            // The command 'l' starts a listening socket (server).
            int port = atoi(message.substr(2).c_str());
            meeter_greeter_ = new MeeterGreeter(this, port);
        }
        break;
    default:
        std::ostringstream status;
        status << "Unhandled message from JavaScript: " << message;
        PostMessage(status.str());
        break;
    }
}

bool httproxy_instance_t::IsConnected() {
    return true;
}

bool httproxy_instance_t::IsUDP() {
  return false;//!udp_socket_.is_null();
}


void httproxy_instance_t::Close() {
    PostMessage("Call to close.");
    if(meeter_greeter_) {
        delete meeter_greeter_;
        meeter_greeter_ = NULL;
    }
}

// The HTTProxyModule provides an implementation of pp::Module that creates
// httproxy_instance_t objects when invoked.
class HTTProxyModule : public pp::Module {
 public:
  HTTProxyModule() : pp::Module() {}
  virtual ~HTTProxyModule() {}

  virtual pp::Instance* CreateInstance(PP_Instance instance) {
    return new httproxy_instance_t(instance);
  }
};

// Implement the required pp::CreateModule function that creates our specific
// kind of Module.
namespace pp {
Module* CreateModule() { return new HTTProxyModule(); }
}  // namespace pp
