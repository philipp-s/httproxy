// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef REQUEST_PROCESSOR_H_
#define REQUEST_PROCESSOR_H_

#include <atomic>
#include <condition_variable>
#include <thread>

#include "http_parser/http_parser.h"
#include "simple_fwd.h"

#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/tcp_socket.h"
#include "ppapi/utility/completion_callback_factory.h"

static const int kBufferSize = 4*1024;

// Simple "echo" server based on a listening pp::TCPSocket.
// This server handles just one connection at a time and will
// echo back whatever bytes get sent to it.
class RequestProcessor : public proxygen::http_parser {
public:
  RequestProcessor(pp::Instance *instance, int socket);
  virtual ~RequestProcessor();
  void run(void);

  static int url_cb (proxygen::http_parser*, const char *at, size_t length);
protected:
  
private:
  pp::Instance *instance_;
  int socket_;

  char receive_buffer_[kBufferSize];
  std::string host_;
  uint16_t port_;
  bool have_url_;
};

#endif  // REQUEST_PROCESSOR_H_
