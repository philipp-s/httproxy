// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#ifndef MEETER_GREETER_H_
#define MEETER_GREETER_H_

#define THREAD_POOL_SIZE (9)

#include "thread_pool.h"

#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/tcp_socket.h"
#include "ppapi/utility/completion_callback_factory.h"

class MeeterGreeter {
 public:
  MeeterGreeter(pp::Instance *instance, uint16_t port);
  virtual ~MeeterGreeter();
#if 0
    :instance_(instance)
    ,callback_factory_(this)
    ,thread_pool_(THREAD_POOL_SIZE) {
    Start(port);
  }
#endif
 protected:
    void run(void);
#if 0
  void Start(uint16_t port);
  void TryAccept();

  // Callback functions
  void OnBindCompletion(int32_t result);
  void OnListenCompletion(int32_t result);
  void OnAcceptCompletion(int32_t result, pp::TCPSocket socket);
#endif
  pp::Instance *instance_;
  int listening_socket_;
  uint16_t port_;
  thread_pool thread_pool_;
};

#endif  // MEETER_GREETER_H_
